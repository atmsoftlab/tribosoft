#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "DAC.h"
#include "GPIO_Init.h"
#include "count.h"
#include "functions.h"
#include "Timer.h"
#include "mb.h"
#include "mbport.h"
#include "stm32_ub_dac.h"

//Number MB register like in program ModBus Poll

extern unsigned short GetMBHoldingRegister(USHORT Number);			// read walue one holding-register
extern void SetMBHoldingRegister(USHORT Number, USHORT Data1);	// set Data1 to holding-reg number

extern void adc_init(void);
extern u16 readADC(u8);

static uint8_t TesterAddress=10;
u16 BaudRate = 9600;
eMBParity Parity = MB_PAR_NONE;
float DAC_T=0.0025;

uint16_t UR_task;
uint16_t DU_DT_task;
uint16_t F;
uint32_t T;
uint16_t N1,nn;
uint16_t N2;
	
int main(void) 
{
  //DAC_Init_();	//	Initialization DAC
	UB_DAC_Init(DUAL_DAC); //	Initialization DAC
	GPIO_Ini();		// 	Initialization GPIO
	Timer_Ini();	// 	Initialization Timer4
	eMBErrorCode eStatus;
	eStatus = eMBInit(  MB_RTU, TesterAddress, 0, BaudRate, Parity); 
	eStatus = eMBEnable();	
	SysTick_Config(SystemCoreClock/100 - 168000);// interrupt every 9ms
	adc_init();
	TIM_Cmd(TIM4, ENABLE);
	NVIC_EnableIRQ(TIM4_IRQn);// Enable interrupt from Tim4
	while(1)
  {
	  if (GetMBHoldingRegister(19) != 0)
		/*Functional mode*/
    {
		TIM_Cmd(TIM4, ENABLE);
		NVIC_EnableIRQ(TIM4_IRQn);// Enable interrupt from Tim4
		Tester_func1(); 
	  }
    else
		/*Manual (debug) mode*/
    {
		UR_task = GetMBHoldingRegister(0);
		if (UR_task>1000) {UR_task=1000;} 			// max 1000V
		DU_DT_task = GetMBHoldingRegister(1);		// secret value

		UB_DAC_SetDAC1(UR_task*4095/1500*1.11666); 
		UB_DAC_SetDAC2(DU_DT_task); 

		SetMBHoldingRegister(16, DAC_GetDataOutputValue(DAC_Channel_1)); 
		SetMBHoldingRegister(17, DAC_GetDataOutputValue(DAC_Channel_2)); 
		F = GetMBHoldingRegister(2);
		N1 = GetMBHoldingRegister(3);
		N2 = GetMBHoldingRegister(4);
		SetMBHoldingRegister(5, GPIO_ReadInputDataBit(INPUT_GPIO, BR1_Pin));
		if (GetMBHoldingRegister(6)==1) {	__Set_Bit(OUTPUT_GPIO,HV_Pin);} 			else { __Reset_Bit(OUTPUT_GPIO,HV_Pin); }
		if (GetMBHoldingRegister(7)==1) {	__Set_Bit(OUTPUT_GPIO,Dsch_Pin);} 		else { __Reset_Bit(OUTPUT_GPIO,Dsch_Pin); }
		if (GetMBHoldingRegister(8)==1) 
			{	
				__Set_Bit(OUTPUT_GPIO,RESET_Pin);
				delay_s(2);
				__Reset_Bit(OUTPUT_GPIO,RESET_Pin);
				SetMBHoldingRegister(8, 0);
			} 		
		else { __Reset_Bit(OUTPUT_GPIO,RESET_Pin); }
		if (GetMBHoldingRegister(9)==1) {	__Set_Bit(OUTPUT_GPIO,UR_Stop_Pin);} 	else { __Reset_Bit(OUTPUT_GPIO,UR_Stop_Pin); }
		SetMBHoldingRegister(10, (readADC(1) * 4500 / 0xfff ));
		nn=GetMBHoldingRegister(13);
		if (GetMBHoldingRegister(15)==1) 
		{ for (int ii=1; ii<=nn; ii++)
			{
			
		__Set_Bit(OUTPUT_GPIO,PULSE_Pin);
		__delay_us(1); 
		__Reset_Bit(OUTPUT_GPIO,PULSE_Pin);
		SetMBHoldingRegister(15, 0);
		__delay_us(100); 
			}
		}
		SetMBHoldingRegister(14, GPIO_ReadInputDataBit(INPUT_GPIO, COV_OPEN_Pin));
    }
  }
}
