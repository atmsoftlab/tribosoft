#include "functions.h"
#include "mb.h"
#include "mbport.h"
#include "count.h"
#include "GPIO_Init.h"
#include "stm32_ub_dac.h"

extern unsigned short GetMBHoldingRegister(USHORT Number);	// read walue one holding-register
extern void SetMBHoldingRegister(USHORT Number, USHORT Data1);	// set Data1 to holding-reg number
extern u16 readADC(u8);

extern uint16_t UR_task;
extern	uint16_t DU_DT_task;
extern	uint16_t F;
extern	uint32_t T;
extern	uint16_t N1;
extern	uint16_t N2;
uint32_t N;

void Tester_func1 (void)
{
	UR_task = GetMBHoldingRegister(0);
	if (UR_task>1000) {UR_task=1000;} 			// max 1000V
	
	DU_DT_task = GetMBHoldingRegister(1);
	F = GetMBHoldingRegister(2);
	N1 = GetMBHoldingRegister(3);
	N2 = GetMBHoldingRegister(4);
	
	
	N = N1*32765 + N2;
	
	//SetMBHoldingRegister(5, 0);
	
	if (GetMBHoldingRegister(19)== 1 )//Test
	{
		SetMBHoldingRegister(5, 0);
		SetMBHoldingRegister(14, 1);
	//point 4	
	__Set_Bit(OUTPUT_GPIO,HV_Pin);	
	//point 5
	//DAC_OUT2
	
	UB_DAC_SetDAC1(UR_task*4095/1500*1.11666);//set voltage	rise UR; data is  UR_task
	UB_DAC_SetDAC2(DU_DT_task);//set DU/DT; data is  DU_DT_task 
	__delay_ms(3);
	//point 6	
	__Reset_Bit(OUTPUT_GPIO,Dsch_Pin);
	//point 7
	__Set_Bit(OUTPUT_GPIO,RESET_Pin);
	__delay_us(10);
	__Reset_Bit(OUTPUT_GPIO,RESET_Pin);
	//point 8
	__Reset_Bit(OUTPUT_GPIO,UR_Stop_Pin);
	//point 9
	//DAC_OUT1
		//UB_DAC_SetDAC1(UR_task*4095/1500*1.11666);//set voltage	rise UR; data is  UR_task 
		delay_s(3);
  //point 10
		//voltage control in 	ADC123_IN1 (UPFB)
	SetMBHoldingRegister(10, (readADC(1) * 4500 / 0xfff )); 
	//SetMBHoldingRegister(10, DAC_GetDataOutputValue(DAC_Channel_2));
	delay_s(3);
	//point 11
	T = 1000 / F; //T in us;
	static uint32_t countN = 0;
	for (uint32_t i = 0; i < N; i++ ){
		
	__Set_Bit(OUTPUT_GPIO,PULSE_Pin);
	__delay_us(0.5);
	__Reset_Bit(OUTPUT_GPIO,PULSE_Pin);
	__delay_us(T - 0.5);
		countN++;
}
	if (countN == N)
  SetMBHoldingRegister(13, 1);
	else SetMBHoldingRegister(13, 0);
	countN = 0;
	//point 12
	__Set_Bit(OUTPUT_GPIO,UR_Stop_Pin);
	//point 13
	__Set_Bit(OUTPUT_GPIO,Dsch_Pin);
	delay_s(3);
		//point 14
		//voltage control in 	ADC123_IN1 (UPFB)
	SetMBHoldingRegister(11, ((readADC(1)* 4500 / 0xfff ))); 
	//point 15
	//point 16
		//read BR1 to BR10
	SetMBHoldingRegister(5, GPIO_ReadInputDataBit(INPUT_GPIO, BR1_Pin));
	//point 17
	
	__Reset_Bit(OUTPUT_GPIO,HV_Pin);
	SetMBHoldingRegister(19, 2);
}
}
