#include "count.h"
#include "mb.h"
#include "mbport.h"
#include "GPIO_Init.h"

extern void SetMBHoldingRegister(USHORT Number, USHORT Data1);	// set Data1 to holding-reg number

/*Delay in us. This function not use, use only macro from "count.h". max us delay = 104755299 us*/
void delay_us (uint32_t delay)
{
    while(delay--);
}
/*Delay in ms. This function not use, use only macro from "count.h". max ms delay = 102261 ms*/ 
void delay_ms (uint32_t delay)
{
  while(delay--);
}
/*Delay in s. This function use. max s delay = 4294967296 s*/
void delay_s(int32_t s)
{
  uint32_t count = 0;
  do
{
  __delay_ms(1000);
  count++;
} while (count != s);
}
/*SysTick handler for modbus poll and interview alarm input*/
void SysTick_Handler(void)
{
    (void) eMBPoll();
	/*
  	SetMBHoldingRegister(10,GPIO_ReadInputDataBit(INPUT_GPIO,UNLVO));			// read input discret IN__PB10[UNLVO]
    SetMBHoldingRegister(11,GPIO_ReadInputDataBit(INPUT_GPIO,FAULT));			// read input discret IN__PB11[FAULT]
	*/
}
