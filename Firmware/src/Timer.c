#include "Timer.h"
#include "stdlib.h"

uint16_t DAC_Value;

void Timer_Ini(void)
	{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    uint16_t PrescalerValue;
    //To get TIM4 counter clock at 42 MHz, the prescaler is computed as follows:
    PrescalerValue = (uint16_t)((SystemCoreClock/2)/TIM4_counter_clock)-1;
    /* TIM4 clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
    /* Time base configuration */
    TIM_TimeBaseStructure.TIM_Period = (TIM4_counter_clock/20) - 1;//20Hz
    TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
    TIM_ITConfig(TIM4, TIM_IT_Update,ENABLE);
    TIM_ARRPreloadConfig(TIM4, DISABLE);
    TIM_Cmd(TIM4, ENABLE);
    
    TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
	}

void TIM4_IRQHandler(void)
{
  static uint16_t DAC_Value_now = 0;
  TIM_ClearITPendingBit(TIM4,TIM_IT_Update);
  DAC_Value_now = DAC_GetDataOutputValue(DAC_Channel_2);
  if (abs(DAC_Value_now - DAC_Value) <= inc)
  {
    DAC_Value_now = DAC_Value;
    DAC_SetChannel2Data(DAC_Align_12b_R, DAC_Value_now);
    NVIC_DisableIRQ(TIM4_IRQn);// Disable interrupt from Tim4
    TIM_Cmd(TIM4, DISABLE);
  }
	  else
		{
			if (DAC_Value_now < DAC_Value)
			{
				DAC_Value_now += inc;
				DAC_SetChannel2Data(DAC_Align_12b_R, DAC_Value_now);
			}
  else
  { 
      DAC_SetChannel2Data(DAC_Align_12b_R, DAC_Value);
  }
  }  
}


	/* -----------------------------------------------------------------------
    In this example TIM3 input clock (TIM3CLK) is set to 2 * APB1 clock (PCLK1), 
    since APB1 prescaler is different from 1.   
      TIM3CLK = 2 * PCLK1  
      PCLK1 = HCLK / 4 
      TIM3CLK = HCLK / 2 = SystemCoreClock /2 (84MHz)
          
    To get TIM3 counter clock at 840000 Hz, the prescaler is computed as follows:
       Prescaler = (TIM3CLK / TIM3 counter clock) - 1
       Prescaler = ((SystemCoreClock /2) /21 MHz) - 1
                                              
    To get TIM3 output clock at 10 KHz, the period (ARR)) is computed as follows:
       ARR = (TIM3 counter clock / TIM3 output clock) - 1
           = 8399 
  ----------------------------------------------------------------------- */  
