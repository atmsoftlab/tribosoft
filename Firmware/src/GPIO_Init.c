#include "GPIO_Init.h"
#include "count.h"

void GPIO_Ini(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;

		/*Enable or disable the AHB1 peripheral clock */
	RCC_AHB1PeriphClockCmd(OUTPUT_RCC|INPUT_RCC|RCC_AHB1Periph_GPIOD|RCC_AHB1Periph_GPIOC ,ENABLE);
	/*configure DAC*/
		
	
	/*Configure GPIO discret OUTPUT*/
	GPIO_InitStruct.GPIO_Pin = RESET_Pin|PULSE_Pin|UR_Stop_Pin|Dsch_Pin|HV_Pin;//|Dsch_Pin|IFP_Pin|VRP_Pin;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(OUTPUT_GPIO, &GPIO_InitStruct);
	
  
  /*Configure GPIO discret OUTPUT for UART*/
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_14;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOD, &GPIO_InitStruct);
  
  /*Configure GPIO discret INPUT*/
  GPIO_InitStruct.GPIO_Pin = BR1_Pin|BR2_Pin|BR3_Pin|BR4_Pin|BR5_Pin|BR6_Pin|BR7_Pin|BR8_Pin|BR9_Pin|BR10_Pin|COV_OPEN_Pin;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(INPUT_GPIO, &GPIO_InitStruct);
	
	/* read dip switches - modbus address
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	//GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOC, &GPIO_InitStruct);*/
}

void Change_discret_out(uint16_t GPIO_Pin,uint8_t Status)
{
  
  switch (GPIO_Pin)
  {
    case RESET_Pin:
      GPIO_WriteBit( OUTPUT_GPIO, RESET_Pin, Status ? Bit_SET : Bit_RESET );
      break;
      case PULSE_Pin:
        GPIO_WriteBit( OUTPUT_GPIO, PULSE_Pin, Status ? Bit_SET : Bit_RESET );
        break;
        case UR_Stop_Pin:
          GPIO_WriteBit( OUTPUT_GPIO, UR_Stop_Pin, Status ? Bit_SET : Bit_RESET );
          break;
          case Dsch_Pin:
            GPIO_WriteBit( OUTPUT_GPIO, Dsch_Pin, Status ? Bit_SET : Bit_RESET );
            break;
            case HV_Pin:
              GPIO_WriteBit( OUTPUT_GPIO, HV_Pin, Status ? Bit_SET : Bit_RESET );
              break;
              /*case Dsch_Pin:
                GPIO_WriteBit( OUTPUT_GPIO, Dsch_Pin, Status ? Bit_SET : Bit_RESET );
                break;
                case IFP_Pin:
                  GPIO_WriteBit( OUTPUT_GPIO, IFP_Pin, Status ? Bit_SET : Bit_RESET );
                  break;
                  case VRP_Pin:
                    GPIO_WriteBit( OUTPUT_GPIO, VRP_Pin, Status ? Bit_SET : Bit_RESET );
                    break;*/
  }
}

/*u8 ParamFromDip(void) 					
{
	//??? nead constant for "testAdress" in main.c
	u8 Dip_Data;
	Dip_Data=0;
	Dip_Data=Dip_Data+GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_0);
	Dip_Data=Dip_Data+GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_1)*2;
	Dip_Data=Dip_Data+GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_2)*4;
	Dip_Data=Dip_Data+GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_3)*8;
	Dip_Data=Dip_Data+GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_4)*16;
	Dip_Data=Dip_Data+GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_5)*32;
	Dip_Data=Dip_Data+GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_6)*64;
	Dip_Data=Dip_Data+GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_7)*128;
	return Dip_Data;
}*/


void UART4SetPush(void)
{
	GPIO_WriteBit(GPIOD, GPIO_Pin_14, Bit_SET);
	GPIO_WriteBit(GPIOD, GPIO_Pin_0, Bit_SET);
}

void UART4SetPoll(void)
{
	delay_us(50000);
	GPIO_WriteBit(GPIOD, GPIO_Pin_14, Bit_RESET);
	GPIO_WriteBit(GPIOD, GPIO_Pin_0, Bit_RESET);
}
