#include "DAC.h"


void DAC_Init_(void)
{
  GPIO_InitTypeDef PIN_DAC;// 'PIN_DAC' structure
  DAC_InitTypeDef DAC_InitStr;
  
  /* DAC channel 2 (DAC_OUT1 = PA.5 DAC_OUT2 = PA.4) configuration */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);// set clock for GPIOA
  PIN_DAC.GPIO_Pin = GPIO_Pin_5|GPIO_Pin_4;
  PIN_DAC.GPIO_Mode = GPIO_Mode_AN;
  PIN_DAC.GPIO_PuPd = GPIO_PuPd_NOPULL;
	PIN_DAC.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_Init(GPIOA, &PIN_DAC);// load 'PIN_DAC' structure into registers
  
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);//Enable clock for DAC
	DAC_StructInit(&DAC_InitStr); //Set default value
	DAC_InitStr.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
  /*with buffer without R_load = inf  Uout_min = 90mV Uout_max = Vdd - 60mV (Vdd = 2960mV)
    with R_load = 5.1k Uout_min = 90mV, Uout_max = Vdd - 150mV (Vdd = 2960mV)
    with R_load = 3.3k Uout_min = 90mV, Uout_max = Vdd - 132mV (Vdd = 2960mV)*/
	DAC_Init(DAC_Channel_2, &DAC_InitStr);// load 'DAC_InitStr' structure into registers
	DAC_Cmd(DAC_Channel_2, ENABLE);//Start DAC
}
