#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"

void pinout_init(void) {
	/* Configure pins
  
     PB0	 ------> GPIO_Output		 
		 PB1	 ------> GPIO_Output
  
		 PD14	 ------> GPIO_Output     управление светодиодами

		 PD0	 ------> GPIO_Output		 управление направлением передачи по UART
	*/
	GPIO_InitTypeDef GPIO_InitStruct;

	/*Enable or disable the AHB1 peripheral clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);

	/*Configure GPIO pin for led*/
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
}


void SetRele(unsigned short Num, uint16_t Data)
{
	switch (Num) {
		case 1:
			DAC_SetChannel2Data(DAC_Align_12b_R, Data);
    //GPIO_WriteBit( GPIOB, GPIO_Pin_0, Status ? Bit_SET : Bit_RESET );
			break;
		case 2:
			GPIO_WriteBit( GPIOB, GPIO_Pin_1, Data ? Bit_SET : Bit_RESET );
			break;
		default:
			break;
	}
}

void UART4SetPush(void)
{
	GPIO_WriteBit(GPIOD, GPIO_Pin_14, Bit_SET);
	GPIO_WriteBit(GPIOD, GPIO_Pin_0, Bit_SET);


}

void UART4SetPoll(void)
{
	GPIO_WriteBit(GPIOD, GPIO_Pin_14, Bit_RESET);
	GPIO_WriteBit(GPIOD, GPIO_Pin_0, Bit_RESET);
}
