#include "mb.h"
#include "mbport.h"

/* modbus registers declaration*/
#define REG_HOLDING_START   40001
#define REG_HOLDING_NREGS   20
static USHORT usRegHoldingStart = REG_HOLDING_START;
static USHORT usRegHoldingBuf[REG_HOLDING_NREGS];

/* #define REG_INPUT_START   40101
#define REG_INPUT_NREGS   4
static USHORT usRegInputStart = REG_INPUT_START;
static USHORT usRegInputBuf[REG_INPUT_NREGS];
*/

eMBErrorCode eMBRegInputCB( uint8_t * pucRegBuffer, USHORT usAddress, USHORT usNRegs )

{
    return MB_ENOREG;
    //return eStatus;
}

eMBErrorCode eMBRegHoldingCB( uint8_t * pucRegBuffer, USHORT usAddress,
                              USHORT usNRegs, eMBRegisterMode eMode )
{
    eMBErrorCode    eStatus = MB_ENOERR;
    int             iRegIndex;

    if( ( usAddress >= REG_HOLDING_START ) &&
        ( usAddress + usNRegs <= REG_HOLDING_START + REG_HOLDING_NREGS ) )
    {
        iRegIndex = ( int )( usAddress - usRegHoldingStart );

        switch ( eMode )
        {
        case MB_REG_READ:
            while( usNRegs > 0 )
            {
                *pucRegBuffer++ = ( uint8_t ) ( usRegHoldingBuf[iRegIndex] >> 8 );
                *pucRegBuffer++ = ( uint8_t ) ( usRegHoldingBuf[iRegIndex] & 0xFF );
                iRegIndex++;
                usNRegs--;
            }
            break;
        case MB_REG_WRITE:
            while( usNRegs > 0 )
            {
                usRegHoldingBuf[iRegIndex] = *pucRegBuffer++ << 8;
                usRegHoldingBuf[iRegIndex] |= *pucRegBuffer++;
                iRegIndex++;
                usNRegs--;
            }
            break;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

	return eStatus;
}

eMBErrorCode eMBRegCoilsCB( uint8_t * pucRegBuffer, USHORT usAddress,
                            USHORT usNCoils, eMBRegisterMode eMode )
{
    return MB_ENOREG;
}

eMBErrorCode eMBRegDiscreteCB( uint8_t * pucRegBuffer, USHORT usAddress,
                               USHORT usNDiscrete )
{
    return MB_ENOREG;
}

void SetMBHoldingRegister(USHORT Number, USHORT Data1)
{
	usRegHoldingBuf[Number]= Data1;

}

unsigned short GetMBHoldingRegister(USHORT Number)
{
	return usRegHoldingBuf[Number];
}

/*
void SetMBInputRegister(USHORT Number, USHORT Data)
{
	usRegInputBuf[Number]= Data;
}

*/

