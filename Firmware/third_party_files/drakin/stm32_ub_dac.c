//--------------------------------------------------------------
// File     : stm32_ub_dac.c
// Datum    : 23.03.2013
// Version  : 1.0
// Autor    : UB
// EMail    : mc-4u(@)t-online.de
// Web      : www.mikrocontroller-4u.de
// CPU      : STM32F4
// IDE      : CooCox CoIDE 1.7.0
// Module   : DAC
// Funktion : DA-Wandler (DAC1 und DAC2)
//
// Hinweis  : DAC1-Ausgang an PA4
//            DAC2-Ausgang an PA5
//--------------------------------------------------------------


//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
#include "stm32_ub_dac.h"


//--------------------------------------------------------------
// ���������� ����������
//--------------------------------------------------------------
DAC_MODE_t akt_dac_mode;


//--------------------------------------------------------------
// ���������� �������
//--------------------------------------------------------------
void P_DAC1_InitIO(void);
void P_DAC1_InitDAC(void);
void P_DAC2_InitIO(void);
void P_DAC2_InitDAC(void);


//--------------------------------------------------------------
// ������������� ���
// ������ : [SINGLE_DAC1, SINGLE_DAC2, DUAL_DAC]
//--------------------------------------------------------------
void UB_DAC_Init(DAC_MODE_t mode)
{
  // ���������� ������ ���
  akt_dac_mode=mode;

  if((mode==SINGLE_DAC1) || (mode==DUAL_DAC)) {
    P_DAC1_InitIO();
    P_DAC1_InitDAC();
    // DAC1 ���������� � 0
    UB_DAC_SetDAC1(0);
  }
  if((mode==SINGLE_DAC2) || (mode==DUAL_DAC)) {
    P_DAC2_InitIO();
    P_DAC2_InitDAC();
    // DAC2 ���������� � 0
    UB_DAC_SetDAC2(0);
  }
}


//--------------------------------------------------------------
// ��������� �������� ��� �� DAC1
// �������� : �� 0 �� 4095
//--------------------------------------------------------------
void UB_DAC_SetDAC1(uint16_t wert)
{
  if(akt_dac_mode==SINGLE_DAC2) return; // ���� �� ���������

  // ������������ �������� = 12 ���
  if(wert>4095) wert=4095;

  // ��������� �������� DAC1 (12 ���, ������� ������)
  DAC_SetChannel1Data(DAC_Align_12b_R, wert);
}


//--------------------------------------------------------------
// ��������� �������� ��� �� DAC2
// �������� : �� 0 �� 4095
//--------------------------------------------------------------
void UB_DAC_SetDAC2(uint16_t wert)
{
  if(akt_dac_mode==SINGLE_DAC1) return; // ���� �� ���������

  // ������������ �������� = 12 ���
  if(wert>4095) wert=4095;

  // ��������� �������� DAC2 (12 ���, ������� ������)
  DAC_SetChannel2Data(DAC_Align_12b_R, wert);
}


//--------------------------------------------------------------
// Stellt gleichzeitig einen DAC-Wert am DAC1 und DAC2 ein
// �������� : �� 0 �� 4095
//--------------------------------------------------------------
void UB_DAC_SetDAC1u2(uint16_t dacwert_1, uint16_t dacwert_2)
{
  if(akt_dac_mode!=DUAL_DAC) return; // ���� �� ���������

  // ������������ �������� = 12 ���
  if(dacwert_1>4095) dacwert_1=4095;
  if(dacwert_2>4095) dacwert_2=4095;

  // ��������� �������� DAC1 � DAC2 (12 ���, ������� ������)
  DAC_SetDualChannelData(DAC_Align_12b_R,dacwert_2,dacwert_1);
}


//--------------------------------------------------------------
// ���������� �������
// ������������� IO ����� ��� DAC1
//--------------------------------------------------------------
void P_DAC1_InitIO(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;

  // ��������� ������������
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

  // ���������������� ���-������ ��� ���������� �����
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
}


//--------------------------------------------------------------
// ���������� �������
// ������������� IO ����� ��� DAC2
//--------------------------------------------------------------
void P_DAC2_InitIO(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;

  // ��������� ������������
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

  // ���������������� ���-������ ��� ���������� �����
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
}


//--------------------------------------------------------------
// ���������� �������
// ������������� DAC1
//--------------------------------------------------------------
void P_DAC1_InitDAC(void)
{
  DAC_InitTypeDef  DAC_InitStructure;

  // ��������� ������������
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);

  // DAC-������������
  DAC_InitStructure.DAC_Trigger=DAC_Trigger_None;
  DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
  DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
  DAC_Init(DAC_Channel_1, &DAC_InitStructure);

  // DAC �����������
  DAC_Cmd(DAC_Channel_1, ENABLE);
}


//--------------------------------------------------------------
// ���������� �������
// ������������� DAC2
//--------------------------------------------------------------
void P_DAC2_InitDAC(void)
{
  DAC_InitTypeDef  DAC_InitStructure;

  // ��������� ������������
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);

  // DAC-������������
  DAC_InitStructure.DAC_Trigger=DAC_Trigger_None;
  DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
  DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
  DAC_Init(DAC_Channel_2, &DAC_InitStructure);

  // DAC �����������
  DAC_Cmd(DAC_Channel_2, ENABLE);
}
