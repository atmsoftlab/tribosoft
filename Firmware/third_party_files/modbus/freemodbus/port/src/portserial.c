/*
 * FreeModbus Library: STM32F4xx Port
 * Copyright (C) 2013 Alexey Goncharov <a.k.goncharov@ctrl-v.biz>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "stm32f4xx_conf.h"
#include "stm32f4xx_usart.h"
#include "port.h"
#include "misc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

void vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )

{
	extern void UART4SetPoll(void);
	extern void UART4SetPush(void);
    if( xRxEnable )
    {

        USART_ITConfig( UART4, USART_IT_RXNE, ENABLE );
        UART4SetPoll();
    }
    else
    {
    	//UART4SetPush();
    	USART_ITConfig( UART4, USART_IT_RXNE, DISABLE );
    }

    if ( xTxEnable )
    {
    	UART4SetPush();
        USART_ITConfig( UART4, USART_IT_TXE, ENABLE );

#ifdef RTS_ENABLE
        RTS_HIGH;
#endif
    }
    else
    {
    	//UART4SetPoll();
        USART_ITConfig( UART4, USART_IT_TXE, DISABLE );
    }
}

BOOL xMBPortSerialInit( UCHAR ucPORT, ULONG ulBaudRate,
                        UCHAR ucDataBits, eMBParity eParity )
{
    NVIC_InitTypeDef        NVIC_InitStructure;
    GPIO_InitTypeDef        GPIO_InitStructure;
    USART_InitTypeDef       USART_InitStructure;
    USART_ClockInitTypeDef  USART_ClockInitStructure;

    /* подавляем предупреждение компилятора о неиспользуемой переменной */
    (void) ucPORT;

    RCC_APB1PeriphClockCmd( RCC_APB1Periph_UART4, ENABLE );
    RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOC, ENABLE );

    GPIO_PinAFConfig( GPIOC, GPIO_PinSource10, GPIO_AF_UART4 );
    GPIO_PinAFConfig( GPIOC, GPIO_PinSource11, GPIO_AF_UART4 );

    GPIO_StructInit( &GPIO_InitStructure );

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init( GPIOC, &GPIO_InitStructure );

    USART_DeInit( UART4 );

    USART_ClockStructInit( &USART_ClockInitStructure );

    USART_ClockInit( UART4, &USART_ClockInitStructure );

    // настройка скорости обмена
    USART_InitStructure.USART_BaudRate = (uint32_t)ulBaudRate;

    // настройка кол-ва битов данных
    if( ucDataBits == 9 )
        USART_InitStructure.USART_WordLength = USART_WordLength_9b;
    else
        USART_InitStructure.USART_WordLength = USART_WordLength_8b;

    // кол-во стоп-битов устанавливаем равным 1
    USART_InitStructure.USART_StopBits = USART_StopBits_1;

    // настройка паритета (по умолчанию - его нет)
    switch( eParity )
    {
    case MB_PAR_NONE:
        USART_InitStructure.USART_Parity = USART_Parity_No;
        break;
    case MB_PAR_ODD:
        USART_InitStructure.USART_Parity = USART_Parity_Odd;
        break;
    case MB_PAR_EVEN:
        USART_InitStructure.USART_Parity = USART_Parity_Even;
        break;
    default:
        USART_InitStructure.USART_Parity = USART_Parity_No;
        break;
    };

    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;

    USART_Init( UART4, &USART_InitStructure );

    NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;           // канал
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;   // приоритет
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;          // приоритет субгруппы
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;             // включаем канал
    NVIC_Init(&NVIC_InitStructure);                             // инициализируем

    USART_Cmd( UART4, ENABLE );

    vMBPortSerialEnable( TRUE, TRUE );

#ifdef RTS_ENABLE
    RTS_INIT;
#endif
    return TRUE;
}

BOOL xMBPortSerialPutByte( CHAR ucByte )
{

    USART_SendData( UART4, (uint16_t) ucByte );
    return TRUE;
}

BOOL xMBPortSerialGetByte( CHAR * pucByte )
{

	*pucByte = (CHAR) USART_ReceiveData( UART4 );
    return TRUE;
}

void UART4_IRQHandler( void )
{


    if ( USART_GetITStatus( UART4, USART_IT_RXNE ) != RESET )
    {

        USART_ClearITPendingBit( UART4, USART_IT_RXNE );
        pxMBFrameCBByteReceived();
    }
    if ( USART_GetITStatus( UART4, USART_IT_TXE ) != RESET )
    {

        USART_ClearITPendingBit( UART4, USART_IT_TXE );
        pxMBFrameCBTransmitterEmpty();
    }
}

