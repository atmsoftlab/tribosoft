#ifndef GPIO_INIT_H
#define GPIO_INIT_H
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

//Define discret OUTPUT
#define RESET_Pin     GPIO_Pin_0
#define PULSE_Pin     GPIO_Pin_1
#define UR_Stop_Pin     GPIO_Pin_2
#define Dsch_Pin   GPIO_Pin_3
#define HV_Pin   GPIO_Pin_4

//Define discret INPUT
#define BR1_Pin   GPIO_Pin_5
#define BR2_Pin   GPIO_Pin_6
#define BR3_Pin   GPIO_Pin_7
#define BR4_Pin   GPIO_Pin_8
#define BR5_Pin   GPIO_Pin_9
#define BR6_Pin   GPIO_Pin_10
#define BR7_Pin   GPIO_Pin_11
#define BR8_Pin   GPIO_Pin_12
#define BR9_Pin   GPIO_Pin_13
#define BR10_Pin   GPIO_Pin_14
#define COV_OPEN_Pin   GPIO_Pin_15 

//Define discret GPIO
#define OUTPUT_RCC  RCC_AHB1Periph_GPIOB
#define INPUT_RCC   RCC_AHB1Periph_GPIOB

#define OUTPUT_GPIO GPIOB
#define INPUT_GPIO  GPIOB

#define __Set_Bit(GPIO,GPIO_Pin) GPIO->BSRRL = GPIO_Pin;
#define __Reset_Bit(GPIO,GPIO_Pin) GPIO->BSRRH = GPIO_Pin;

/*Prototipe functions*/
void GPIO_Ini(void);
void Change_discret_out(uint16_t GPIO_Pin,uint8_t Status);
void UART4SetPush(void);
void UART4SetPoll(void);

#endif
