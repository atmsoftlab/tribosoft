#ifndef TIMER_H
#define TIMER_H
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#define TIM4_counter_clock  840000
#define inc 50

void Timer_Ini(void);
void TIM4_IRQHandler(void);

#endif
