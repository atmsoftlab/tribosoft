function DeviceLoaded = LoadDevicesIni(devices)
% ��������� .ini ����� ��������� � ��������� � ������ devN, ��� N- id ����������
% � ��������� ��� ��������� � base workspace
DeviceLoaded=devices;

for i=1:length(devices)
    inif=[num2str(devices(i,1)) '.ini'];
    s1=ini2struct(inif);
    assignin('base',['dev' num2str(devices(i,1))], s1) ;
    logThis(['Loading ' ['dev' num2str(devices(i,1))] ]);
end


