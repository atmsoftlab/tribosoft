function [ConnectedDev] = TestConnect()
%TESTCONNECT Summary of this function goes here
%   Detailed explanation goes here

%s=SerialInit(InitS);
% �������� ������ .ini ������ ���������
InitS=ini2struct('morion.ini');
fileList = getAllFiles([InitS.Path.RootFolderPath InitS.Path.INIFilesPath]);

%������� ������ ��������� � ������� - ����� � ������� 1.ini ... 255.ini
index=0;
if ~isempty(fileList)
    for i=1:255
        for j=1:length(fileList)
            inif=['\' num2str(i) '.ini'];
            if strfind(fileList{j},inif)>0
                index=index+1;
                devices(index,1)=i;
            end;
        end;
    end;
    logThis('Readind devices .ini files');
    for i=1:length(devices)
        logThis(['Reading ' num2str(devices(i)) '.ini' ]);
    end;
    assignin('base','devices', devices);
%��������� .ini ����� ��������� � ��������� � ������ devN ��� N- id ����������
    LoadDevicesIni(devices); 
else
    logThis(['! Devices .ini files not found in target directory' ]);
    devices=fileList;
end;

%todo!!
%�������� ����������� ��������� ����� �� ������ 
%0 - ����� ������ !=0 - ������ ������ ������� ������

%devices(:,2)=-1;
%s=SerialInit(evalin('base','InitS'));
for i=1:length(devices)
   dev=['dev' num2str(devices(i,1)) '.Modbus.BaseAddress'];
   [status, ~]=ModbusRead_3(s,devices(i,1),evalin('base',dev),1);
   devices(i,2)=status;
   logThis(['Connect with device ' num2str(devices(i,1)) ' status = ' num2str(status) ]);
%    if (status~=0)       % ������� ��������� �� ���������� ���������
%        clear(['dev' num2str(devices(i,1))]);
%    end;    
end;
k=1;
ConnectedDev=-1;
for i=1:length(devices)
    if(devices(i,2)==0)
        ConnectedDev(k)=devices(i,1);
        k=k+1;
    end;
end;
ConnectedDev;
%SerialDeInit(s);

