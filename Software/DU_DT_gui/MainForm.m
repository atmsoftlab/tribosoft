function varargout = MainForm(varargin)
% MAINFORM MATLAB code for MainForm.fig
%      MAINFORM, by itself, creates a new MAINFORM or raises the existing
%      singleton*.
%
%      H = MAINFORM returns the handle to a new MAINFORM or the handle to
%      the existing singleton*.
%
%      MAINFORM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAINFORM.M with the given input arguments.
%
%      MAINFORM('Property','Value',...) creates a new MAINFORM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MainForm_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MainForm_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MainForm

% Last Modified by GUIDE v2.5 09-Jun-2018 16:11:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MainForm_OpeningFcn, ...
                   'gui_OutputFcn',  @MainForm_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MainForm is made visible.
function MainForm_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MainForm (see VARARGIN)

% Choose default command line output for MainForm
handles.output = hObject;
global start_const;
start_const = 1;
global dataInput_flag;
dataInput_flag = 0;
% Update handles structure
guidata(hObject, handles);



set(handles.tBR1, 'String', '��������');
%set(handles.tBR2, 'String', '��������');
%set(handles.tBR3, 'String', '��������');
%set(handles.tBR4, 'String', '��������');
%set(handles.tBR5, 'String', '��������');
%set(handles.tBR6, 'String', '��������');
%set(handles.tBR7, 'String', '��������');
%set(handles.tBR8, 'String', '��������');
%set(handles.tBR9, 'String', '��������');
%set(handles.tBR10, 'String', '��������');
set(handles.messages, 'String', '���������');
set(handles.tUPFB1, 'String', '0B');
set(handles.tUPFB2, 'String', '0B');
set(handles.tNcontrol, 'String', '0');
% UIWAIT makes MainForm wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = MainForm_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function connect(hObject, eventdata, handles)
connect_flag = check_connect();
global check_connect_flag;
if connect_flag == 1
	set(handles.messages, 'String', '���������. ���������� �����������.');
	check_connect_flag = 1
	pause(2);
else
	set(handles.messages, 'String', '����������� ���������� � �������!!! ��������� ����������� � ������������� ���������.');
	
	choice = questdlg('���� ���������� ��������� � �����������, ������� "��", ���� ������ ��������� ������ ������� "���"', '����������� ���������� � �������.', ...
    '��','�e�','�e�');
% Handle response
	switch choice
		case '�e�'
			%exit;
			check_connect_flag = 0
		case '��'
			connect(hObject, eventdata, handles);
			
	end
end


% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Exit_Callback(hObject, eventdata, handles)
% hObject    handle to Exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

choice = questdlg('�� ������������� ������ ��������� ������?', '���������� ������', ...
    '��','���','���');
% Handle response
switch choice
    case '��'
    exit;         % todo kill matlab also
    %close;
    case '���'
end




% --------------------------------------------------------------------
function Options_Callback(hObject, eventdata, handles)
% hObject    handle to Options (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function SerialPortOptions_Callback(hObject, eventdata, handles)
% hObject    handle to SerialPortOptions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global s;
SerialDeInit(s);


% --------------------------------------------------------------------
function Options_Load_Callback(hObject, eventdata, handles)
% hObject    handle to Options_Load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile({'*.ini','ini-files (*.ini)';  '*.*',  'All Files (*.*)'},'�������� ���������� *.ini ���� ��������', 'MultiSelect', 'off');
if (filename~=0)
    assignin('base','InitS',ini2struct(filename))
end;

% --------------------------------------------------------------------
function RunScript_Callback(hObject, eventdata, handles)
% hObject    handle to RunScript (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile({'*.m','m-script files (*.m)';  '*.*',  'All Files (*.*)'},'�������� ���������� ������� ��� �������', 'MultiSelect', 'on');
a=['run(''' pathname filename ''')'];
%a=['run(''' filename ''')'];
if isempty(filename)~=0
    eval(a)
end;


% --------------------------------------------------------------------
function DebugWrk_Callback(hObject, eventdata, handles)
% hObject    handle to DebugWrk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function OptSave_Callback(hObject, eventdata, handles)
% hObject    handle to OptSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Help_Callback(hObject, eventdata, handles)
% hObject    handle to Help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function About_Callback(hObject, eventdata, handles)
% hObject    handle to About (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
About();
pause(30);


% --------------------------------------------------------------------
function Update_Callback(hObject, eventdata, handles)
% hObject    handle to Update (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Documentation_Callback(hObject, eventdata, handles)
% hObject    handle to Documentation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function LogFileopen_Callback(hObject, eventdata, handles)
% hObject    handle to LogFileopen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Log();

% --------------------------------------------------------------------
function TestConnect_Callback(hObject, eventdata, handles)
% hObject    handle to TestConnect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
TestConnect();


% --------------------------------------------------------------------
function DataBaseConnect_Callback(hObject, eventdata, handles)
% hObject    handle to DataBaseConnect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if TestDBConnect()==1
    logThis('database connected ok');
else
    logThis('! database connected error');
end;


% --------------------------------------------------------------------
function Dev10_Callback(hObject, eventdata, handles)
% hObject    handle to Dev10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
deviceform10();


%DMITRIJ



%���� ������

function tUR_task_Callback(hObject, eventdata, handles)
% hObject    handle to tUR_task (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tUR_task as text
%        str2double(get(hObject,'String')) returns contents of tUR_task as a double


% --- Executes during object creation, after setting all properties.
function tUR_task_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tUR_task (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in pF_task.
function pF_task_Callback(hObject, eventdata, handles)
% hObject    handle to pF_task (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pF_task contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pF_task


% --- Executes during object creation, after setting all properties.
function pF_task_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pF_task (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global F_task_arr;
F_task_arr = [1, 2, 5, 10];
set(hObject,'String',F_task_arr);
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tN_task_Callback(hObject, eventdata, handles)
% hObject    handle to tN_task (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tN_task as text
%        str2double(get(hObject,'String')) returns contents of tN_task as a double


% --- Executes during object creation, after setting all properties.
function tN_task_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tN_task (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function set_outData(Pool, handles, hObject, eventdata)
%doids
if Pool(6) == 0
set(handles.tBR1, 'String', '��������');
logThis('test object is OK');
else
set(handles.tBR1, 'String', '������');
logThis('test object is broken');
end
%if Pool(7) == 0
%set(handles.tBR2, 'String', '��������');
%else
%set(handles.tBR2, 'String', '������');
%end
%if Pool(8) == 0
%set(handles.tBR3, 'String', '��������');
%else
%set(handles.tBR3, 'String', '������');
%end
%if Pool(9) == 0
%set(handles.tBR4, 'String', '��������');
%else
%set(handles.tBR4, 'String', '������');
%end
%if Pool(10) == 0
%set(handles.tBR5, 'String', '��������');
%else
%set(handles.tBR5, 'String', '������');
%end
%if Pool(16) == 0
%set(handles.tBR6, 'String', '��������');
%else
%set(handles.tBR6, 'String', '������');
%end
%if Pool(17) == 0
%set(handles.tBR7, 'String', '��������');
%else
%set(handles.tBR7, 'String', '������');
%end
%if Pool(18) == 0
%set(handles.tBR8, 'String', '��������');
%else
%set(handles.tBR8, 'String', '������');
%end
%if Pool(19) == 0
%set(handles.tBR9, 'String', '��������');
%else
%set(handles.tBR9, 'String', '������');
%end
%if Pool(20) == 0
%set(handles.tBR10, 'String', '��������');
%else
%set(handles.tBR10, 'String', '������');
%end
%contol data
set(handles.tUPFB1, 'String', num2str(Pool(11)));
logThis(['Test voltage control before test ' [num2str(Pool(11)) 'V']]);
set(handles.tUPFB2, 'String', num2str(Pool(12))); 
logThis(['Test voltage control after test ' [num2str(Pool(12)) 'V']]);
% if Pool(14) == 1
if 1== 1
set(handles.tNcontrol, 'String', get(handles.tN_task,'String'));
else
    set(handles.tNcontrol, 'String', '�� ���');
end


% --- Executes on button press in dataInput.
function dataInput(hObject, eventdata, handles)
% hObject    handle to dataInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global DUDT_task_arr;
global F_task_arr;
%index = get(handles.tDUDT_task,'Value');
index1 = get(handles.pF_task,'Value');
global DUDT_task;
global F_task;
global N_task;
global UR_task;
global dataInput_flag;
%DUDT_task = uint16(DUDT_task_arr(index));
DUDT_task = uint16(str2double(get(handles.tDUDT_task,'String')));
F_task = uint16(F_task_arr(index1));
N_task = uint32(str2double(get(handles.tN_task,'String')));
UR_task = uint16(str2double(get(handles.tUR_task,'String')));
set(handles.messages, 'String', '������ �������');
dataInput_flag = 1;



% --- Executes on button press in pB_START.
function pB_START_Callback(hObject, eventdata, handles)
% hObject    handle to pB_START (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
connect(hObject, eventdata, handles);
pause(1);
global check_connect_flag;
if check_connect_flag == 0
	flag = 1
	exit;
end
dataInput(hObject, eventdata, handles)

global DUDT_task;
global F_task;
global N_task;
global UR_task;
global dataInput_flag;
global start_const;
if dataInput_flag < 1
set(handles.messages, 'String', '��������� ������, ����������!');
Start_flag = 0;
else
dataInput_flag = 0;
set(handles.tBR1, 'String', '���������');
%set(handles.tBR2, 'String', '���������');
%set(handles.tBR3, 'String', '���������');
%set(handles.tBR4, 'String', '���������');
%set(handles.tBR5, 'String', '���������');
%set(handles.tBR6, 'String', '���������');
%set(handles.tBR7, 'String', '���������');
%set(handles.tBR8, 'String', '���������');
%set(handles.tBR9, 'String', '���������');
%set(handles.tBR10, 'String', '���������');
set(handles.tUPFB1, 'String', '0B');
set(handles.tUPFB2, 'String', '0B');
set(handles.tNcontrol, 'String', '0');
set(handles.messages, 'String', '������ ���������');
logThis('Test started');
pause(1);
if strcmp(get(handles.messages, 'String'), '������ ���������')
Start_flag = start_const;
else
Start_flag = 0;
end
end
global N_task1;
global N_task2;
global connect2device_flag;
global s;
if Start_flag == 1    
connect2device_flag = connect2device(UR_task, DUDT_task, F_task, N_task, 1);
end
pause(1);
[status, Pool] = ModbusRead_3(s,10,evalin('base','40000'),20); 
if connect2device_flag == 0
    set(handles.messages, 'String', '������ �������� ������!!! ��������������� ���������, ����������.'); 
elseif Pool(20) == 2
set(handles.messages, 'String', '��������� �������� �������.');
logThis('Test finished');
set_outData(Pool,handles, hObject, eventdata);
%elseif Pool(13) == 2
%set(handles.messages, 'String', '������!!!');
%set_outData(Pool,handles, hObject, eventdata);
end




% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over messages.
function messages_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to messages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over tUPFB1.
function tUPFB1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to tUPFB1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over tUPFB2.
function tUPFB2_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to tUPFB2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over tBR1.
function tBR1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to tBR1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over tBR2.
function tBR2_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to tBR2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over tBR3.
function tBR3_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to tBR3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over tBR4.
function tBR4_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to tBR4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over tBR5.
function tBR5_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to tBR5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over tBR6.
function tBR6_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to tBR6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over tBR7.
function tBR7_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to tBR7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over tBR8.
function tBR8_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to tBR8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over tBR9.
function tBR9_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to tBR9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over tBR10.
function tBR10_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to tBR10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over tNcontrol.
function tNcontrol_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to tNcontrol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function test_Callback(hObject, eventdata, handles)
% hObject    handle to test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global start_const;
start_const = 0



function tDUDT_task_Callback(hObject, eventdata, handles)
% hObject    handle to tDUDT_task (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tDUDT_task as text
%        str2double(get(hObject,'String')) returns contents of tDUDT_task as a double


% --- Executes during object creation, after setting all properties.
function tDUDT_task_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tDUDT_task (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function Test_serial_Callback(hObject, eventdata, handles)
% hObject    handle to Test_serial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
S_tests();


% --------------------------------------------------------------------
function Test_serial_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to Test_serial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
