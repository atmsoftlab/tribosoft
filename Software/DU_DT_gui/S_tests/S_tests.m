function varargout = S_tests(varargin)
% S_TESTS MATLAB code for S_tests.fig
%      S_TESTS, by itself, creates a new S_TESTS or raises the existing
%      singleton*.
%
%      H = S_TESTS returns the handle to a new S_TESTS or the handle to
%      the existing singleton*.
%
%      S_TESTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in S_TESTS.M with the given input arguments.
%
%      S_TESTS('Property','Value',...) creates a new S_TESTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before S_tests_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to S_tests_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help S_tests

% Last Modified by GUIDE v2.5 13-Jun-2018 11:31:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @S_tests_OpeningFcn, ...
                   'gui_OutputFcn',  @S_tests_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before S_tests is made visible.
function S_tests_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to S_tests (see VARARGIN)

% Choose default command line output for S_tests
handles.output = hObject;
global DATA_Set_flag;
DATA_Set_flag = 0;
% Update handles structure
guidata(hObject, handles);


set(handles.tBR, 'String', '��������');
set(handles.Messages, 'String', '���������');
set(handles.tURFB_1, 'String', '0B');
set(handles.tURFB_2, 'String', '0B');
set(handles.tN_control, 'String', '0');
% UIWAIT makes S_tests wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = S_tests_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function connect(hObject, eventdata, handles)
connect_flag = check_connect();
global check_connect_flag;
if connect_flag == 1
	set(handles.Messages, 'String', '���������. ���������� �����������.');
	check_connect_flag = 1
	pause(2);
else
	set(handles.Messages, 'String', '����������� ���������� � �������!!! ��������� ����������� � ������������� ���������.');
	
	choice = questdlg('���� ���������� ��������� � �����������, ������� "��", ���� ������ ��������� ������ ������� "���"', '����������� ���������� � �������.', ...
    '��','�e�','�e�');
% Handle response
	switch choice
		case '�e�'
			%exit;
			check_connect_flag = 0
		case '��'
			connect(hObject, eventdata, handles);
			
	end
end
% --- Executes on selection change in checkParam.
function checkParam_Callback(hObject, eventdata, handles)
% hObject    handle to checkParam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns checkParam contents as cell array
%        contents{get(hObject,'Value')} returns selected item from checkParam


% --- Executes during object creation, after setting all properties.
function checkParam_CreateFcn(hObject, eventdata, handles)
% hObject    handle to checkParam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global param_arr;
param_arr = {'�������� �������������� ����������'; '�������� ���������� ����������'; '���������� ������������� ���������'};
set(hObject,'String',param_arr);
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pF_task.
function pF_task_Callback(hObject, eventdata, handles)
% hObject    handle to pF_task (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pF_task contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pF_task


% --- Executes during object creation, after setting all properties.
function pF_task_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pF_task (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global F_task_arr;
F_task_arr = [1, 2, 5, 10];
set(hObject,'String',F_task_arr);
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function UR_0_Callback(hObject, eventdata, handles)
% hObject    handle to UR_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of UR_0 as text
%        str2double(get(hObject,'String')) returns contents of UR_0 as a double


% --- Executes during object creation, after setting all properties.
function UR_0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to UR_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function UR_Step_Callback(hObject, eventdata, handles)
% hObject    handle to UR_Step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of UR_Step as text
%        str2double(get(hObject,'String')) returns contents of UR_Step as a double


% --- Executes during object creation, after setting all properties.
function UR_Step_CreateFcn(hObject, eventdata, handles)
% hObject    handle to UR_Step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function UR_H_Callback(hObject, eventdata, handles)
% hObject    handle to UR_H (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of UR_H as text
%        str2double(get(hObject,'String')) returns contents of UR_H as a double


% --- Executes during object creation, after setting all properties.
function UR_H_CreateFcn(hObject, eventdata, handles)
% hObject    handle to UR_H (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function DUDT_0_Callback(hObject, eventdata, handles)
% hObject    handle to DUDT_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DUDT_0 as text
%        str2double(get(hObject,'String')) returns contents of DUDT_0 as a double


% --- Executes during object creation, after setting all properties.
function DUDT_0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DUDT_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function DUDT_Step_Callback(hObject, eventdata, handles)
% hObject    handle to DUDT_Step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DUDT_Step as text
%        str2double(get(hObject,'String')) returns contents of DUDT_Step as a double


% --- Executes during object creation, after setting all properties.
function DUDT_Step_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DUDT_Step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function DUDT_H_Callback(hObject, eventdata, handles)
% hObject    handle to DUDT_H (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DUDT_H as text
%        str2double(get(hObject,'String')) returns contents of DUDT_H as a double


% --- Executes during object creation, after setting all properties.
function DUDT_H_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DUDT_H (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function N_0_Callback(hObject, eventdata, handles)
% hObject    handle to N_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of N_0 as text
%        str2double(get(hObject,'String')) returns contents of N_0 as a double


% --- Executes during object creation, after setting all properties.
function N_0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to N_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function N_Step_Callback(hObject, eventdata, handles)
% hObject    handle to N_Step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of N_Step as text
%        str2double(get(hObject,'String')) returns contents of N_Step as a double


% --- Executes during object creation, after setting all properties.
function N_Step_CreateFcn(hObject, eventdata, handles)
% hObject    handle to N_Step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function N_H_Callback(hObject, eventdata, handles)
% hObject    handle to N_H (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of N_H as text
%        str2double(get(hObject,'String')) returns contents of N_H as a double


% --- Executes during object creation, after setting all properties.
function N_H_CreateFcn(hObject, eventdata, handles)
% hObject    handle to N_H (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function UR_now_Callback(hObject, eventdata, handles)
% hObject    handle to UR_now (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of UR_now as text
%        str2double(get(hObject,'String')) returns contents of UR_now as a double


% --- Executes during object creation, after setting all properties.
function UR_now_CreateFcn(hObject, eventdata, handles)
% hObject    handle to UR_now (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function DUDT_now_Callback(hObject, eventdata, handles)
% hObject    handle to DUDT_now (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DUDT_now as text
%        str2double(get(hObject,'String')) returns contents of DUDT_now as a double


% --- Executes during object creation, after setting all properties.
function DUDT_now_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DUDT_now (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function N_now_Callback(hObject, eventdata, handles)
% hObject    handle to N_now (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of N_now as text
%        str2double(get(hObject,'String')) returns contents of N_now as a double


% --- Executes during object creation, after setting all properties.
function N_now_CreateFcn(hObject, eventdata, handles)
% hObject    handle to N_now (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%Functions

% --- Executes on button press in Data_set.
function Data_set(hObject, eventdata, handles)
indexF = get(handles.pF_task, 'Value');
global indexParam;
indexParam = get(handles.checkParam, 'Value');

global DUDT_0;global DUDT_Step;global DUDT_H;
global N_0;global N_Step;global N_H;
global UR_0;global UR_Step;global UR_H;
global delay; global F_task;
global F_task_arr;

F_task = uint16(F_task_arr(indexF));
DUDT_0 = int16(str2double(get(handles.DUDT_0,'String')));
DUDT_Step = int16(str2double(get(handles.DUDT_Step,'String')));
DUDT_H = int16(str2double(get(handles.DUDT_H,'String')));
UR_0 = int16(str2double(get(handles.UR_0,'String')));
UR_Step = int16(str2double(get(handles.UR_Step,'String')));
UR_H = int16(str2double(get(handles.UR_H,'String')));
N_0 = int32(str2double(get(handles.N_0,'String')));
N_Step = int32(str2double(get(handles.N_Step,'String')));
N_H = int32(str2double(get(handles.N_H,'String')));
delay = int16(str2double(get(handles.delay,'String')));

global DUDT_arr; global UR_arr; global N_arr;

if DUDT_H >= DUDT_0
DUDT_arr = DUDT_0:DUDT_Step:DUDT_H;
else
DUDT_arr = DUDT_0:-DUDT_Step:DUDT_H;
end
if UR_H >= UR_0
UR_arr = UR_0:UR_Step:UR_H;
else
UR_arr = UR_0:-UR_Step:UR_H;
end
if N_H >= N_0
N_arr = N_0:N_Step:N_H;
else
N_arr = N_0:-N_Step:N_H;
end
global DATA_Set_flag;
DATA_Set_flag = 1;
set(handles.Messages, 'String', '������ �������');


% --- Executes on button press in S_testStart.
function S_testStart_Callback(hObject, eventdata, handles)

connect(hObject, eventdata, handles);
pause(1);
global check_connect_flag;
if check_connect_flag == 0
	flag = 1
	exit;
end
Data_set(hObject, eventdata, handles);

global DUDT_0;global DUDT_Step;global DUDT_H;
global N_0;global N_Step;global N_H;
global UR_0;global UR_Step;global UR_H;
global delay;
global F_task_arr;
global DATA_Set_flag;
if DATA_Set_flag < 1
set(handles.Messages, 'String', '��������� ������, ����������!');
global Start_flag;
Start_flag = 0;
else
DATA_Set_flag = 0;
set(handles.tBR, 'String', '���������');
set(handles.tURFB_1, 'String', '0B');
set(handles.tURFB_2, 'String', '0B');
set(handles.tN_control, 'String', '0');
set(handles.Messages, 'String', '������ ���������');
%logThis('Test started');
global Start_flag;
Start_flag = 1;
pause(1);
end

if Start_flag == 1
global indexParam;
global increment_param_arr;
switch indexParam
	case 1,
		increment_param_arr = [1,0,0];
	case 2,
		increment_param_arr = [0,1,0];
	case 3,
		increment_param_arr = [0,0,1];
	otherwise,
		increment_param_arr = [0,0,0];
end
Serial_Tests_set(hObject, eventdata, handles);
end
% --- Executes on button press in STOP.
function STOP_Callback(hObject, eventdata, handles)
global Start_flag;
Start_flag = 0;


function set_outData(Pool, handles, hObject, eventdata)
%doids
if Pool(6) == 0
set(handles.tBR, 'String', '��������');
logThis('test object is OK');
else
set(handles.tBR, 'String', '������');
global Start_flag;
Start_flag = 0;
logThis('test object is broken');
end

set(handles.tURFB_1, 'String', num2str(Pool(11)));
logThis(['Test voltage control before test ' [num2str(Pool(11)) 'V']]);
set(handles.tURFB_2, 'String', num2str(Pool(12))); 
logThis(['Test voltage control after test ' [num2str(Pool(12)) 'V']]);

function Serial_Tests_set(hObject, eventdata, handles)
global DUDT_arr; global UR_arr; global N_arr;global delay; global F_task;
global Start_flag;
global indexParam;
global s;
global increment_param_arr;
switch indexParam
	case 1,
		count_tests = length(UR_arr)
	case 2,
		count_tests = length(DUDT_arr)
	case 3,
		count_tests = length(N_arr)
	otherwise,
		increment_param_arr = 1;
end
for i = 0:(count_tests-1)
	if Start_flag == 1
		%����� ������� �������� ����������
		set(handles.UR_now, 'String', int2str(UR_arr(i*increment_param_arr(1)+1)));
		set(handles.DUDT_now, 'String', int2str(DUDT_arr(i*increment_param_arr(2)+1)));
		set(handles.N_now, 'String', int2str(N_arr(i*increment_param_arr(3)+1)));
		set(handles.tBR, 'String', '���������');
		pause(1);
		connect2device_flag = connect2device(uint16(UR_arr(i*increment_param_arr(1)+1)), uint16(DUDT_arr(i*increment_param_arr(2)+1)), F_task, uint16(N_arr(i*increment_param_arr(3)+1)), 1);
		[status, Pool] = ModbusRead_3(s,10,evalin('base','40000'),20); 
		pause(0.5);
		%connect2device = 1;
		set_outData(Pool, handles, hObject, eventdata);
		pause(0.5);
		if connect2device_flag == 1 && i < count_tests-1
			set(handles.Messages, 'String', '������� ��������� ��������, ������������� �������� �����.');
			pause(delay);
			set(handles.Messages, 'String', '��������� ��������� ������, �������� ���������');
			pause(1);
		else 
			set(handles.Messages, 'String', '����� ��������� ��������.');
		end
	else
		set(handles.Messages, 'String', '����� ��������� ��������.');
	end
end
	

function delay_Callback(hObject, eventdata, handles)
% hObject    handle to delay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of delay as text
%        str2double(get(hObject,'String')) returns contents of delay as a double


% --- Executes during object creation, after setting all properties.
function delay_CreateFcn(hObject, eventdata, handles)
% hObject    handle to delay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
