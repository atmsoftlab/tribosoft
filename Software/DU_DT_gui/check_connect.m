function connect_flag = check_connect()

global s;
ModbusWrite_6(s,10,evalin('base','40019'),123);
pause(1);
[status, Pool]=ModbusRead_3L(s,10,evalin('base','40019'),1); 
if Pool(1)==123
	connect_flag = 1;
else
	connect_flag = 0;
end

end
