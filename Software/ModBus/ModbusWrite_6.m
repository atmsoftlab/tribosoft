function Status=ModbusWrite_6(S,Id,Address,Data)
% Status=ModbusWrite_6(s,id,Address,Data)
% s - Serial Port Object
% id - modbus slave device ID
% Address - base address + offset for changed holding-registers
% Data - Data to write
MBcommand= 6;                        % modbus function to access
logThis('ModbusWrite_6 started (id/MBCommand/baseaddr/Data)= %d/%d/%d/%d',...
    Id, MBcommand,Address,Data);
Len_packet=8;
addr_hi=fix(Address/256);
addr_lo=Address-addr_hi*256;
Data_hi=fix(Data/256);
Data_lo=Data-Data_hi*256;

Message = [Id MBcommand addr_hi addr_lo Data_hi Data_lo];
Message = Append_CRC(Message);
fwrite(S, Message);                 % transmit message
Response = fread(S, 8);    % get response
len_r=length(Response);
if len_r>3                          % length response > 3
    if (Response(2)==MBcommand)         % slave device not say "Error"
        if Check_CRC(Response)==0   % CRC OK
            Status=0;
        end;
    else
        Status=Response(2);         % Error code from slave device 
    end;
else
    Status=2;
end;
logThis('ModbusWrite_6 closed, exit status = %d', Status);

