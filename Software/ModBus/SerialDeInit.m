function status = SerialDeInit(s)
%Close serial connection
fclose(s)
logThis(strcat(s.Name,' closed'));
status=0;

