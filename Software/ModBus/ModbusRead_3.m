function [status, Pool]=ModbusRead_3(s,id,address,NRegs)
% this function reads NRegs registers from port s device id
% [status, Pool]=Modbus_3(s,id,NRegs)
% s - Serial Port Object
% id - modbus slave device ID
% NRegs - number registers to read
% address - base address
MBcommand=3;                        % modbus function to access
logThis('ModbusRead_3 started (id/MBCommand/baseaddr/NRegs)= %d/%d/%d/%d',...
    id, MBcommand,address,NRegs);
addr_hi=fix(address/256);
addr_lo=address-addr_hi*256;
NRegs_hi=fix(NRegs/256);
NRegs_lo=NRegs-NRegs_hi*256;
Message = [id MBcommand addr_hi addr_lo NRegs_hi NRegs_lo];
Message = Append_CRC(Message);
fwrite(s, Message);                 % transmit message
Response = fread(s, NRegs*2+5);     % get response
mm2=zeros(NRegs,1);
len_r=length(Response);
if len_r>3                          % length response > 3
    if (Response(2)==MBcommand)     % slave device not say "Error"
        if Check_CRC(Response)==0   % CRC OK
            mm1=Response(3:len_r-2);
            kk=0;
            for ii=2:2:length(mm1)
                mm2(kk+1,1)=mm1(ii)*256+mm1(ii+1);
                kk=kk+1;
            end;
            Pool=mm2;
            status=0;
        end;
    else
        status=Response(2);         % Error code from slave device 
    end;
else
    Pool=zeros(5,1);
    status=2;
end;
logThis('ModbusRead_3 exit status = %2d',status);

