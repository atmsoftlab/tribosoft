function [s] = SerialInit(InitS)
% Initialize serial port on specified com port
% InitS - structure inported from .ini file

s = serial(InitS.SerialPort.Port);

% Specify connection parameters
set(s,'BaudRate',InitS.SerialPort.BaudRate,'DataBits',InitS.SerialPort.DataBits,'StopBits',InitS.SerialPort.StopBits,'Parity',InitS.SerialPort.Parity,'Timeout',InitS.SerialPort.Timeout);

%Open serial connection
fopen(s);
logThis(strcat(s.Name,' is open'));
% Specify Terminator - not used for binary mode (RTU) writing
% if strcmp(InitS.ModBus.Mode,'ASCII')
%     s.terminator = 'CR/LF';
% else
    s.terminator = '';
% end;

% Set read mode
set(s,InitS.SerialPort.ReadSyncMode,InitS.SerialPort.Continuous);


