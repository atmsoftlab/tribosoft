function Check = Check_CRC(response)
receivedMsg = response;
challengeMsg = Append_CRC(response(1:length(response)-2));
if ((receivedMsg-challengeMsg) ~=0)
    Check = 1;
else
    Check = 0;
end