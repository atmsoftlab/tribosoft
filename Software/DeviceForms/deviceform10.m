function varargout = deviceform10(varargin)
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @deviceform10_OpeningFcn, ...
                   'gui_OutputFcn',  @deviceform10_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before deviceform10 is made visible.
function deviceform10_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to deviceform10 (see VARARGIN)

% Choose default command line output for deviceform10
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes deviceform10 wait for user response (see UIRESUME)
% uiwait(handles.figure1);
a=evalin('base','AddrConnectedDevices');
flag=0;
for i=1:length(a)
if a(i)==10                % 10 - ����� �������
    flag=1;                 % ������ ��������
end;
end;

if flag==1
handles.text3.String=evalin('base','dev10.Device.DeviceName');  %��������� ����� �� .ini �����
aa=SetTableData();
aa(1:length(evalin('base','dev10.Registers.NumRegs')),3)=SetRead(evalin('base','s'));
assignin('base','devtable', aa)
set(handles.uitable2, 'Data',aa);% �������� ����������� ������� �� �����

t=timer;
setGlobalx(t);
t.ExecutionMode='fixedRate';
t.Period=3;
t.TimerFcn = @(myTimerObj, thisEvent)refresh10(handles);
start(t);
% todo - �������� ��� ���������� ����� ������� �������
else
    choice = questdlg('���������� �� ����������', '���������� �� ����������', ...
    'OK', '���������','���������');
    close;
end;


function a=SetTableData()
b=evalin('base','dev10.Registers.NumRegs'); % ������ �������� �������� � ��������� ������ ����������
strings=length(b);          % ��������� ����� �����
a=cell(strings,5);          % �������� ������� �����
% �������� �������� ������� ��������� ������� 1 ������� �������
for i=1:strings 
    a{i,1}=num2str(b(i));    
end;
% �������� �������� ������� ��������� ������� 2 ������� �������
for i=1:strings 
     a{i,2}=evalin('base',['dev10.Registers.Name' num2str(i)]);    
end;
% �������� �������� ������� ��������� ������� 4 ������� ������� (��. ���.)
for i=1:strings 
     a{i,4}=evalin('base',['dev10.Registers.Unit' num2str(i)]);    
end;

function r=SetRead(s)
d=evalin('base','dev10.Modbus.ModBusID');
A=evalin('base','dev10.Modbus.BaseAddress');
R=evalin('base','dev10.Registers.NumRegs');
strings=length(R);
[F Zn]=ModbusRead_3(evalin('base','s'),d,A,strings);
r=cell(strings,1);
for i=1:strings
    r{i,1}=Zn(i);
end;

function WriteValue(s,handles)
d=evalin('base','dev10.Modbus.ModBusID');
A=evalin('base','dev10.Modbus.BaseAddress');
[m,n]=size(get(handles.uitable2,'Data'));
p=get(handles.uitable2,'Data');
for i=1:m
    if ~isempty(p{i,n})
        l=p{i,n};
        F=ModbusWrite_6(evalin('base','s'),d,A+i-1,l);
    end
end    

% --- Outputs from this function are returned to the command line.
function varargout = deviceform10_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when entered data in editable cell(s) in uitable2.
function uitable2_CellEditCallback(hObject, eventdata, handles)
%s=SerialInit(evalin('base','InitS'));
WriteValue(evalin('base','s'),handles);
% hObject    handle to uitable2 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
%set(handles.uitable2, 'Data',WriteValue(s,handles));
%SerialDeInit(s);


% --- Executes on button press in refresh.
function refresh_Callback(hObject, eventdata, handles)
% hObject    handle to refresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
refresh10(handles);

function  refresh10(handles)
aa=evalin('base','devtable');
aa(1:length(evalin('base','dev10.Registers.NumRegs')),3)=SetRead(evalin('base','s'));
assignin('base','devtable', aa)
set(handles.uitable2, 'Data',aa);% �������� ����������� ������� �� �����

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
